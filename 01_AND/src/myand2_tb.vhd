----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 09/07/2020 07:28:39 PM
-- Design Name: 
-- Module Name: myand_2_tb - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity myand2_tb is
--  Port ( );
end myand2_tb;

architecture Behavioral of myand2_tb is
component myand2 is
  Port ( a : in STD_LOGIC;
         b : in STD_LOGIC;
         c : out STD_LOGIC);
end component;
signal a_sig : STD_LOGIC := '0';
signal b_sig : STD_LOGIC := '0';
signal c_sig : STD_LOGIC;
begin
uut: myand2 PORT MAP (
a => a_sig,
b => b_sig,
c => c_sig
);

stim_proc: process
begin		
wait for 100 ns;	
a_sig <= '1';
b_sig <= '0';
wait for 10 ns;
a_sig <= '1';
b_sig <= '1';
wait for 10 ns;
a_sig <= '0';
b_sig <= '1';
wait for 10 ns;
a_sig <= '0';
b_sig <= '0';
wait;
end process;
end Behavioral;
