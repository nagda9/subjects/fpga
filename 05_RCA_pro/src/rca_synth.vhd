LIBRARY IEEE;
USE ieee.std_logic_1164.ALL;
USE ieee.numeric_std.ALL;

ENTITY rca_synth IS
    GENERIC (width : INTEGER);
    PORT (
        a : IN std_logic_vector(width - 1 DOWNTO 0);
        b : IN std_logic_vector(width - 1 DOWNTO 0);
        cin : IN std_logic;
        s : OUT std_logic_vector(width - 1 DOWNTO 0);
        cout : OUT std_logic
    );
END ENTITY rca_synth;

ARCHITECTURE behave OF rca_synth IS
    SIGNAL tmp : unsigned(width DOWNTO 0);
    SIGNAL tmp_cin : unsigned(0 DOWNTO 0);
BEGIN
    tmp_cin(0) <= cin;
    tmp <= resize(unsigned(a), width + 1) + unsigned(b) + tmp_cin;
    s <= std_logic_vector(tmp(width - 1 DOWNTO 0));
    cout <= tmp(width);
END ARCHITECTURE behave;