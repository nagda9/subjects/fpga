LIBRARY IEEE;
USE ieee.std_logic_1164.ALL;
USE ieee.numeric_std.ALL;

ENTITY rca_tb IS
END ENTITY rca_tb;

ARCHITECTURE behave OF rca_tb IS
    COMPONENT rca IS
        PORT (
            a : IN std_logic_vector(3 DOWNTO 0);
            b : IN std_logic_vector(3 DOWNTO 0);
            cin : IN std_logic;
            s : OUT std_logic_vector(3 DOWNTO 0);
            cout : OUT std_logic
        );
    END COMPONENT rca;
    SIGNAL a : std_logic_vector(3 DOWNTO 0);
    SIGNAL b : std_logic_vector(3 DOWNTO 0);
    SIGNAL cin : std_logic;
    SIGNAL s : std_logic_vector(3 DOWNTO 0);
    SIGNAL cout : std_logic;
BEGIN
    uut : rca PORT MAP(
        a => a,
        b => b,
        cin => cin,
        s => s,
        cout => cout
    );

    stimulus : PROCESS
        VARIABLE result : INTEGER;
        VARIABLE all_res, good_res : INTEGER := 0;
        VARIABLE s_exp : std_logic_vector(3 DOWNTO 0);
        VARIABLE cout_exp : std_logic;
    BEGIN
        WAIT FOR 100 ns;
        FOR c_stim IN 0 TO 1 LOOP
            FOR b_stim IN 0 TO 15 LOOP
                FOR a_stim IN 0 TO 15 LOOP
                    all_res := all_res + 1;
                    -- Set input
                    a <= std_logic_vector(to_unsigned(a_stim, 4));
                    b <= std_logic_vector(to_unsigned(b_stim, 4));
                    cin <= std_logic(to_unsigned(c_stim, 1)(0));
                    WAIT FOR 10 ns;
                    -- Compute and check result
                    REPORT "Inputs a :" & to_string(a) & " b: " & to_string(b) & " c: " & std_logic'image(cin);
                    result := a_stim + b_stim + c_stim;
                    s_exp := std_logic_vector(to_unsigned(result, 5)(3 DOWNTO 0));
                    cout_exp := std_logic(to_unsigned(result, 5)(4));
                    IF (s = s_exp AND cout = cout_exp) THEN
                        good_res := good_res + 1;
                    END IF;
                    ASSERT s = s_exp REPORT "Error! s: " & to_string(s) & " /= " & to_string(s_exp) SEVERITY error;
                    ASSERT cout = cout_exp REPORT "Error! cout: " & to_string(cout) & " /= " & std_logic'image(cout_exp) SEVERITY error;
                END LOOP;
            END LOOP;
        END LOOP;
        REPORT "All tests: " & to_string(all_res) & " Failed tests: " & INTEGER'image(all_res - good_res);
        WAIT FOR 100 ns;
        WAIT;
    END PROCESS stimulus;
END ARCHITECTURE behave;