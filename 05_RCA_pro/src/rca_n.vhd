LIBRARY IEEE;
USE ieee.std_logic_1164.ALL;
USE ieee.numeric_std.ALL;

ENTITY rca_n IS
    GENERIC (width : INTEGER);
    PORT (
        a : IN std_logic_vector(width - 1 DOWNTO 0);
        b : IN std_logic_vector(width - 1 DOWNTO 0);
        cin : IN std_logic;
        s : OUT std_logic_vector(width - 1 DOWNTO 0);
        cout : OUT std_logic
    );
END ENTITY rca_n;

ARCHITECTURE behave OF rca_n IS
    COMPONENT fulladder IS
        PORT (
            a : IN std_logic;
            b : IN std_logic;
            cin : IN std_logic;
            s : OUT std_logic;
            cout : OUT std_logic
        );
    END COMPONENT fulladder;
    SIGNAL carry : std_logic_vector(width - 2 DOWNTO 0);
BEGIN
    fa_bits : FOR i IN 0 TO width - 1 GENERATE
        fa_first : IF i = 0 GENERATE
            bit0 : fulladder PORT MAP(
                a => a(i),
                b => b(i),
                cin => cin,
                s => s(i),
                cout => carry(i)
            );
        END GENERATE;
        fa_middle : IF i > 0 AND i < width - 1 GENERATE
            bitm : fulladder PORT MAP(
                a => a(i),
                b => b(i),
                cin => carry(i - 1),
                s => s(i),
                cout => carry(i)
            );
        END GENERATE;
        fa_last : IF i = width - 1 GENERATE
            bitn : fulladder PORT MAP(
                a => a(i),
                b => b(i),
                cin => carry(i - 1),
                s => s(i),
                cout => cout
            );
        END GENERATE;
    END GENERATE;
END ARCHITECTURE behave;