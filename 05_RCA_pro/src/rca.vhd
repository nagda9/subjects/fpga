LIBRARY IEEE;
USE ieee.std_logic_1164.ALL;
USE ieee.numeric_std.ALL;

ENTITY rca IS
    PORT (
        a : IN std_logic_vector(3 DOWNTO 0);
        b : IN std_logic_vector(3 DOWNTO 0);
        cin : IN std_logic;
        s : OUT std_logic_vector(3 DOWNTO 0);
        cout : OUT std_logic
    );
END ENTITY rca;

ARCHITECTURE behave OF rca IS
    COMPONENT fulladder IS
        PORT (
            a : IN std_logic;
            b : IN std_logic;
            cin : IN std_logic;
            s : OUT std_logic;
            cout : OUT std_logic
        );
    END COMPONENT fulladder;
    SIGNAL carry : std_logic_vector(2 DOWNTO 0);
BEGIN
    bit0 : fulladder PORT MAP(
        a => a(0),
        b => b(0),
        cin => cin,
        s => s(0),
        cout => carry(0)
    );

    bit1 : fulladder PORT MAP(
        a => a(1),
        b => b(1),
        cin => carry(0),
        s => s(1),
        cout => carry(1)
    );
    bit2 : fulladder PORT MAP(
        a => a(2),
        b => b(2),
        cin => carry(1),
        s => s(2),
        cout => carry(2)
    );
    bit3 : fulladder PORT MAP(
        a => a(3),
        b => b(3),
        cin => carry(2),
        s => s(3),
        cout => cout
    );

END ARCHITECTURE behave;