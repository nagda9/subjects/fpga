LIBRARY IEEE;
USE ieee.std_logic_1164.ALL;

ENTITY fulladder IS
    PORT (
        a : IN std_logic;
        b : IN std_logic;
        cin : IN std_logic;
        s : OUT std_logic;
        cout : OUT std_logic
    );
END ENTITY fulladder;

ARCHITECTURE behave OF fulladder IS
BEGIN
    s <= a XOR b XOR cin;
    cout <= (a AND b) OR (cin AND (a OR b));
END ARCHITECTURE behave;