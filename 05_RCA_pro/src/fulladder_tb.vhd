LIBRARY IEEE;
USE ieee.std_logic_1164.ALL;
USE ieee.numeric_std.ALL;

ENTITY fulladder_tb IS
END ENTITY fulladder_tb;

ARCHITECTURE behave OF fulladder_tb IS
    COMPONENT fulladder IS
        PORT (
            a : IN std_logic;
            b : IN std_logic;
            cin : IN std_logic;
            s : OUT std_logic;
            cout : OUT std_logic
        );
    END COMPONENT fulladder;
    SIGNAL a : std_logic;
    SIGNAL b : std_logic;
    SIGNAL cin : std_logic;
    SIGNAL s : std_logic;
    SIGNAL cout : std_logic;
BEGIN
    uut : fulladder PORT MAP(
        a => a,
        b => b,
        cin => cin,
        s => s,
        cout => cout
    );

    stimulus : PROCESS
        VARIABLE result : INTEGER;
        VARIABLE all_res, good_res : INTEGER := 0;
        VARIABLE s_exp, cout_exp : std_logic;
    BEGIN
        WAIT FOR 100 ns;
        FOR c_stim IN 0 TO 1 LOOP
            FOR b_stim IN 0 TO 1 LOOP
                FOR a_stim IN 0 TO 1 LOOP
                    all_res := all_res + 1;
                    -- Set input
                    a <= std_logic(to_unsigned(a_stim, 1)(0));
                    b <= std_logic(to_unsigned(b_stim, 1)(0));
                    cin <= std_logic(to_unsigned(c_stim, 1)(0));
                    WAIT FOR 10 ns;
                    -- Compute and check result
                    REPORT "Inputs a :" & std_logic'image(a) & " b: " & std_logic'image(b) & " c: " & std_logic'image(cin);
                    result := a_stim + b_stim + c_stim;
                    s_exp := std_logic(to_unsigned(result, 2)(0));
                    cout_exp := std_logic(to_unsigned(result, 2)(1));
                    IF (s = s_exp AND cout = cout_exp) THEN
                        good_res := good_res + 1;
                    END IF;
                    ASSERT s = s_exp REPORT "Error! s: " & std_logic'image(s) & " /= " & std_logic'image(s_exp) SEVERITY error;
                    ASSERT cout = cout_exp REPORT "Error! cout: " & std_logic'image(cout) & " /= " & std_logic'image(cout_exp) SEVERITY error;
                END LOOP;
            END LOOP;
        END LOOP;
        REPORT "All tests: " & INTEGER'image(all_res) & " Failed tests: " & INTEGER'image(all_res - good_res);
        WAIT FOR 100 ns;
        WAIT;
    END PROCESS stimulus;
END ARCHITECTURE behave;