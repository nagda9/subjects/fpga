----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 12/01/2016 09:49:47 AM
-- Design Name: 
-- Module Name: pmod_gyro_vga_bar - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------
LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
USE IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

ENTITY fir_example IS
	PORT (
		clk : IN STD_LOGIC;
		adin : IN STD_LOGIC_VECTOR (15 DOWNTO 0);
		daout : OUT STD_LOGIC_VECTOR (36 DOWNTO 0);
		adin_vld : IN STD_LOGIC;
		daout_vld : OUT STD_LOGIC);
END fir_example;

ARCHITECTURE Behavioral OF fir_example IS
	CONSTANT FIR_LENGTH : INTEGER := 25;
	TYPE fir_ram_type IS ARRAY (0 TO 31) OF STD_LOGIC_VECTOR (15 DOWNTO 0);
	SIGNAL smem : fir_ram_type;
	CONSTANT cmem : fir_ram_type := (
		X"0001", X"0002", X"0003", X"0004", X"0005", X"0006", X"0007", X"0008", OTHERS => (OTHERS => '0')
		-- others => (X"0001")
		-- others => (X"7fff")
		-- OTHERS => STD_LOGIC_VECTOR(to_signed((2 ** 15)/25, 16))
		-- X"0001", X"0002", X"0003", X"0004", X"0005", X"0006", X"0007", X"0008", X"0009", X"000a", X"000b", X"000c", X"000d",
		-- X"000c", X"000b", X"000a", X"0009", X"0008", X"0007", X"0006", X"0005", X"0004", X"0003", X"0002", X"0001",
		-- others => (others => '0')
	);
	SIGNAL smem_out_reg : STD_LOGIC_VECTOR(15 DOWNTO 0);
	SIGNAL cmem_out_reg : STD_LOGIC_VECTOR(15 DOWNTO 0);
	SIGNAL i_adr : unsigned(4 DOWNTO 0) := (OTHERS => '0');
	SIGNAL s_adr : unsigned(4 DOWNTO 0);
	SIGNAL c_adr : unsigned(4 DOWNTO 0);
	SIGNAL fir_adin : STD_LOGIC_VECTOR(15 DOWNTO 0) := (OTHERS => '0');
	SIGNAL fir_daout : STD_LOGIC_VECTOR(36 DOWNTO 0);
	SIGNAL x_reg : signed(15 DOWNTO 0);
	SIGNAL y_reg : signed(15 DOWNTO 0);
	SIGNAL p_reg : signed(36 DOWNTO 0);
	TYPE fir_state_type IS (idle, init, store_sample, store_sample_wait, load, load_wait, mul, mac, res_write, res_vld);
	SIGNAL fir_state : fir_state_type := idle;
	SIGNAL s_mem_we : STD_LOGIC;
	SIGNAL i_adr_dec : STD_LOGIC;--decrement i_adr
	SIGNAL s_adr_load : STD_LOGIC;
	SIGNAL s_adr_incr : STD_LOGIC;
	SIGNAL c_adr_clr : STD_LOGIC;
	SIGNAL c_adr_incr : STD_LOGIC;
	SIGNAL x_reg_en : STD_LOGIC;
	SIGNAL y_reg_en : STD_LOGIC;
	SIGNAL p_reg_mul : STD_LOGIC;
	SIGNAL p_reg_mac : STD_LOGIC;
	SIGNAL fir_daout_en : STD_LOGIC;

BEGIN

	PROCESS (clk)
	BEGIN
		IF rising_edge(clk) THEN
			--FIR state machine
			CASE fir_state IS
				WHEN idle =>
					IF adin_vld = '1' THEN
						fir_state <= init;
					END IF;
				WHEN init =>
					fir_state <= store_sample;
				WHEN store_sample =>
					fir_state <= store_sample_wait;
				WHEN store_sample_wait =>
					fir_state <= load;
				WHEN load =>
					fir_state <= load_wait;
				WHEN load_wait =>
					fir_state <= mul;
				WHEN mul =>
					fir_state <= mac;
				WHEN mac =>
					IF c_adr = FIR_LENGTH THEN
						fir_state <= res_write;
					ELSE
						fir_state <= mac;
					END IF;
				WHEN res_write =>
					fir_state <= res_vld;
				WHEN res_vld =>
					fir_state <= idle;
				WHEN OTHERS =>
					NULL;
			END CASE;
			--Datapath elements
			IF adin_vld = '1' THEN
				fir_adin <= adin;
			END IF;
			smem_out_reg <= smem(to_integer(s_adr));
			IF s_mem_we = '1' THEN
				smem(to_integer(s_adr)) <= fir_adin;
			END IF;
			cmem_out_reg <= cmem(to_integer(c_adr));
			IF i_adr_dec = '1' THEN
				i_adr <= i_adr - 1;
			END IF;
			IF s_adr_load = '1' THEN
				s_adr <= i_adr;
			ELSIF s_adr_incr = '1' THEN
				s_adr <= s_adr + 1;
			END IF;
			IF c_adr_clr = '1' THEN
				c_adr <= (OTHERS => '0');
			ELSIF c_adr_incr = '1' THEN
				c_adr <= c_adr + 1;
			END IF;
			IF x_reg_en = '1' THEN
				x_reg <= signed(smem_out_reg);
			END IF;
			IF y_reg_en = '1' THEN
				y_reg <= signed(cmem_out_reg);
			END IF;
			IF p_reg_mul = '1' THEN
				p_reg <= resize(x_reg * y_reg, p_reg'length);
			ELSIF p_reg_mac = '1' THEN
				p_reg <= p_reg + x_reg * y_reg;
			END IF;
			IF fir_daout_en = '1' THEN
				-- fir_daout <= STD_LOGIC_VECTOR(resize(p_reg(p_reg'high DOWNTO 15), 16));
				fir_daout <= STD_LOGIC_VECTOR(p_reg);
			END IF;
			--Control signals
			IF fir_state = init THEN
				i_adr_dec <= '1';
			ELSE
				i_adr_dec <= '0';
			END IF;
			IF fir_state = init THEN
				s_adr_load <= '1';
			ELSE
				s_adr_load <= '0';
			END IF;
			IF fir_state = load OR fir_state = load_wait OR fir_state = mul OR fir_state = mac THEN
				s_adr_incr <= '1';
			ELSE
				s_adr_incr <= '0';
			END IF;
			IF fir_state = store_sample THEN
				s_mem_we <= '1';
			ELSE
				s_mem_we <= '0';
			END IF;
			IF fir_state = init THEN
				c_adr_clr <= '1';
			ELSE
				c_adr_clr <= '0';
			END IF;
			IF fir_state = load OR fir_state = load_wait OR fir_state = mul OR fir_state = mac THEN
				c_adr_incr <= '1';
			ELSE
				c_adr_incr <= '0';
			END IF;
			IF fir_state = load_wait OR fir_state = mul OR fir_state = mac THEN
				x_reg_en <= '1';
			ELSE
				x_reg_en <= '0';
			END IF;
			IF fir_state = load_wait OR fir_state = mul OR fir_state = mac THEN
				y_reg_en <= '1';
			ELSE
				y_reg_en <= '0';
			END IF;
			IF fir_state = mul THEN
				p_reg_mul <= '1';
			ELSE
				p_reg_mul <= '0';
			END IF;
			IF fir_state = mac THEN
				p_reg_mac <= '1';
			ELSE
				p_reg_mac <= '0';
			END IF;
			IF fir_state = res_write THEN
				fir_daout_en <= '1';
			ELSE
				fir_daout_en <= '0';
			END IF;
			IF fir_state = res_vld THEN
				daout_vld <= '1';
			ELSE
				daout_vld <= '0';
			END IF;
		END IF;
	END PROCESS;
END Behavioral;