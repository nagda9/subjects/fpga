----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 11/16/2020 07:38:10 PM
-- Design Name: 
-- Module Name: fir_example_tb - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------
LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
USE IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

ENTITY fir_example_tb IS
	--  Port ( );
END fir_example_tb;

ARCHITECTURE Behavioral OF fir_example_tb IS
	COMPONENT fir_example IS
		PORT (
			clk : IN STD_LOGIC;
			adin : IN STD_LOGIC_VECTOR (15 DOWNTO 0);
			daout : OUT STD_LOGIC_VECTOR (36 DOWNTO 0);
			adin_vld : IN STD_LOGIC;
			daout_vld : OUT STD_LOGIC);
	END COMPONENT;

	SIGNAL clk : STD_LOGIC;
	SIGNAL adin : STD_LOGIC_VECTOR (15 DOWNTO 0) := (OTHERS => '0');
	SIGNAL adin_vld : STD_LOGIC := '0';

	SIGNAL daout : STD_LOGIC_VECTOR (36 DOWNTO 0);
	SIGNAL daout_vld : STD_LOGIC;

	PROCEDURE fir_input(data_in : IN INTEGER; SIGNAL clk : IN STD_LOGIC; SIGNAL adin : OUT STD_LOGIC_VECTOR (15 DOWNTO 0); SIGNAL adin_vld : OUT STD_LOGIC) IS
	BEGIN
		WAIT UNTIL falling_edge(clk);
		adin_vld <= '1';
		adin <= STD_LOGIC_VECTOR(to_signed(data_in, 16));
		WAIT UNTIL falling_edge(clk);
		adin_vld <= '0';
		adin <= (OTHERS => '0');
	END fir_input;
	PROCEDURE wait_for_result(SIGNAL clk : IN STD_LOGIC; SIGNAL daout_vld : IN STD_LOGIC) IS
	BEGIN
		WAIT UNTIL daout_vld = '1';
		WAIT UNTIL falling_edge(clk);
	END wait_for_result;
	--function eval_fir(sample : integer) return signed is
	--begin
	--	smem <= to_signed(sample,16) & smem(0 to smem'high-1);
	--end eval_fir;
BEGIN

	check : PROCESS
		TYPE fir_ram_type IS ARRAY (0 TO 31) OF signed(15 DOWNTO 0);
		VARIABLE smem : fir_ram_type;
		CONSTANT cmem : fir_ram_type := (
			X"0001", X"0002", X"0003", X"0004", X"0005", X"0006", X"0007", X"0008", OTHERS => (OTHERS => '0')
			-- others => (X"0001")
			-- others => (X"7fff")
			-- OTHERS => STD_LOGIC_VECTOR(to_signed((2 ** 15)/25, 16))
			-- X"0001", X"0002", X"0003", X"0004", X"0005", X"0006", X"0007", X"0008", X"0009", X"000a", X"000b", X"000c", X"000d",
			-- X"000c", X"000b", X"000a", X"0009", X"0008", X"0007", X"0006", X"0005", X"0004", X"0003", X"0002", X"0001",
			-- others => (others => '0')
		);
		VARIABLE tmp_p : signed(36 DOWNTO 0);
	BEGIN
		WAIT UNTIL adin_vld = '1';
		WAIT UNTIL rising_edge(clk);
		smem := signed(adin) & smem(0 TO smem'high - 1);
		tmp_p := to_signed(0, 37);
		FOR i IN 0 TO 24 LOOP
			tmp_p := tmp_p + smem(i) * cmem(i);
		END LOOP;
		WAIT UNTIL daout_vld = '1';
		WAIT UNTIL rising_edge(clk);
		IF tmp_p /= signed(daout) THEN
			REPORT "Error";
		ELSE
			REPORT "Ok";
		END IF;
	END PROCESS;

	stim : PROCESS
	BEGIN
		WAIT FOR 100 ns;

		-- WAIT UNTIL falling_edge(clk);
		-- adin_vld <= '1';
		-- adin <= STD_LOGIC_VECTOR(to_signed(5, 16));
		-- WAIT UNTIL falling_edge(clk);
		-- adin_vld <= '0';
		-- adin <= (OTHERS => '0');

		-- WAIT UNTIL daout_vld = '1';
		-- WAIT UNTIL falling_edge(clk);
		-- WAIT UNTIL falling_edge(clk);

		-- adin_vld <= '1';
		-- adin <= STD_LOGIC_VECTOR(to_signed(6, 16));
		-- WAIT UNTIL falling_edge(clk);
		-- adin_vld <= '0';
		-- adin <= (OTHERS => '0');

		-- fir_input(5, clk, adin, adin_vld);
		-- wait_for_result(clk, daout_vld);
		-- fir_input(6, clk, adin, adin_vld);
		-- wait_for_result(clk, daout_vld);

		FOR i IN 5 TO 40 LOOP
			fir_input(i, clk, adin, adin_vld);
			-- res_ref = eval_fir(i);
			wait_for_result(clk, daout_vld);
		END LOOP;

		WAIT FOR 100 ns;
		WAIT;
	END PROCESS;

	clk_gen : PROCESS
	BEGIN
		clk <= '0';
		WAIT FOR 5 ns;
		clk <= '1';
		WAIT FOR 5 ns;
	END PROCESS;

	uut : fir_example PORT MAP(
		clk => clk,
		adin => adin,
		daout => daout,
		adin_vld => adin_vld,
		daout_vld => daout_vld);

END Behavioral;