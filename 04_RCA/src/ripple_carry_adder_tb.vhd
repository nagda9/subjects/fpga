library ieee;
use ieee.std_logic_1164.all;
 
entity ripple_carry_adder_tb is
end ripple_carry_adder_tb;
 
architecture behavior of ripple_carry_adder_tb is
 
 -- Component Declaration for the Unit Under Test (UUT)
 
component ripple_carry_adder
    port(
        A : in std_logic_vector(3 downto 0);
        B : in std_logic_vector(3 downto 0);
        Cin : in std_logic;
        S : out std_logic_vector(3 downto 0);
        Cout : out std_logic);
end component;
 
-- Inputs
signal A : std_logic_vector(3 downto 0) := (others => '0');
signal B : std_logic_vector(3 downto 0) := (others => '0');
signal Cin : std_logic := '0';
 
-- Outputs
signal S : std_logic_vector(3 downto 0);
signal Cout : std_logic;
 
begin
 
 -- Initiate the Unit Under Test (UUT)
uut: ripple_carry_adder port map (
    A => A,
    B => B,
    Cin => Cin,
    S => S,
    Cout => Cout);
 
 -- Stimulus process
stimulus: process
begin
    wait for 100 ns;
    A <= "0110";
    B <= "1100";
    
    wait for 100 ns;
    A <= "1111";
    B <= "1100";
    
    wait for 100 ns;
    A <= "0110";
    B <= "0111";
    
    wait for 100 ns;
    A <= "0110";
    B <= "1110";
    
    wait for 100 ns;
    A <= "1111";
    B <= "1111";
    wait for 100 ns;

    wait;
end process;
end;