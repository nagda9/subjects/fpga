library ieee;
use ieee.std_logic_1164.all;
 
entity ripple_carry_adder is
    port ( 
        A : in STD_LOGIC_VECTOR (3 downto 0);
        B : in STD_LOGIC_VECTOR (3 downto 0);
        Cin : in STD_LOGIC;
        S : out STD_LOGIC_VECTOR (3 downto 0);
        Cout : out STD_LOGIC);
end ripple_carry_adder;
 
architecture behave of ripple_carry_adder is
 
 -- Full Adder VHDL Code Component Declaration
    component fulladder
    port (
        A : in STD_LOGIC;
        B : in STD_LOGIC;
        Cin : in STD_LOGIC;
        S : out STD_LOGIC;
        Cout : out STD_LOGIC);
    end component;
 
 -- Intermediate Carry declaration
    signal c1,c2,c3: STD_LOGIC;
 
begin
 -- Port Mapping Full Adder 4 times
    FA1: fulladder port map(
        A => A(0),
        B => B(0), 
        Cin => Cin, 
        S => S(0),
        Cout => c1);
    FA2: fulladder port map(
        A => A(1),
        B => B(1),
        Cin => c1,
        S => S(1),
        Cout => c2);
    FA3: fulladder port map(
        A => A(2),
        B => B(2), 
        Cin => c2, 
        S => S(2), 
        Cout => c3);
    FA4: fulladder port map(
        A => A(3), 
        B => B(3), 
        Cin => c3, 
        S => S(3), 
        Cout => Cout);
end behave;