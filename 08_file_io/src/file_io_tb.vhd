LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE STD.textio.ALL;

ENTITY file_io_tb IS
END file_io_tb;

ARCHITECTURE behavioral OF file_io_tb IS
    FILE out_file : text;
    CONSTANT sig : STD_LOGIC_vector := "000";
BEGIN
    makefile : PROCESS
        VARIABLE out_line : line;
    BEGIN
        WAIT FOR 100 ns;
        file_open(out_file, "onebit.txt", write_mode);
        write(out_line, sig, right, 12);
        writeline(out_file, out_line);

        -- write after each other without newline (write concatenates)
        write(out_line, sig, left); -- same as write(out_line, sig);
        write(out_line, sig);
        writeline(out_file, out_line);

        -- write output and add an empty line
        write(out_line, sig);
        write(out_line, lf);
        writeline(out_file, out_line);

        -- write signal, start new line, add empty line, start new line => 2 empty lines
        write(out_line, sig);
        writeline(out_file, out_line);
        write(out_line, lf);
        writeline(out_file, out_line);

        file_close(out_file);

        -- http://www.webbplats.se/hdc/hdl/educaton/types/exampl.htm  => check out special characters, like 'lf' and 'cr'
        WAIT;
    END PROCESS; -- makefile

END ARCHITECTURE; -- behavioral