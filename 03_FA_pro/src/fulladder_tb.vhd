library IEEE;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity fulladder_tb is
end entity fulladder_tb;

architecture behave of fulladder_tb is
    component fulladder is
        port (
            a : in std_logic;
            b : in std_logic;
            cin : in std_logic;
            s : out std_logic;
            cout : out std_logic
        );
    end component fulladder;
    signal a : std_logic;
    signal b : std_logic;
    signal cin :  std_logic;
    signal s : std_logic;
    signal cout : std_logic;
begin
    uut: fulladder port map (
        a => a,
        b => b,
        cin => cin,
        s => s,
        cout => cout
    );

stimulus : process
variable result : integer;
variable all_res, good_res : integer := 0;
variable s_exp, cout_exp : std_logic;
begin
    wait for 100 ns;
    for c_stim in 0 to 1 loop
        for b_stim in 0 to 1 loop
            for a_stim in 0 to 1 loop
                all_res := all_res + 1;
                -- Set input
                a <= std_logic(to_unsigned(a_stim,1)(0));
                b <= std_logic(to_unsigned(b_stim,1)(0));
                cin <= std_logic(to_unsigned(c_stim,1)(0));
                wait for 10 ns;
                -- Compute and check result
                report "Inputs a :" & std_logic'image(a) & " b: " & std_logic'image(b) & " c: " & std_logic'image(cin);
                result := a_stim + b_stim + c_stim;
                s_exp := std_logic(to_unsigned(result,2)(0));
                cout_exp := std_logic(to_unsigned(result,2)(1));
                if (s = s_exp and cout = cout_exp) then
                    good_res := good_res + 1;
                end if;
                assert s = s_exp report "Error! s: " & std_logic'image(s) & " /= " & std_logic'image(s_exp) severity error;
                assert cout = cout_exp report "Error! cout: "  & std_logic'image(cout) & " /= " & std_logic'image(cout_exp) severity error;
            end loop;
        end loop;
    end loop;
    report "All tests: " & integer'image(all_res) & " Failed tests: " & integer'image(all_res - good_res);
    wait for 100 ns;
    wait;
end process stimulus;
end architecture behave;