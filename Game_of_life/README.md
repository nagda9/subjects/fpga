# Életjáték kód további javítása:
- Az input a testbench-ben legyen megírva, ne a programkódban.
- Az input beadása vector adagokban legyen skálázhatóság miatt.
- OFL ne órához, hanem state-hez legyen kötve.
- OFL szeparációja a kódban.
- Megszabadulni a változóktól.
- Minden pixel egyszer legyen csak betöltve.

# Szómagyarázat
- FSM: Finite State Machine
    1) SM: State Memory
        - FF: flipflops
        - n db FF --> 2^n state
    2) NFL: Next State Logic
        - implemented with combinational logic (parallel) --> logic for FF inputs
        - results in sequential logic (transition from start state to goal state)
    3) OFL: Output Function Logic
        - MOORE: f(state)
        - MEALY: f(state, inputs)

- DCM: Digital Clock Management
- TPD: Time Propagation Delay
- CE: Clock Enable

# Tippek:
- 1 if i jel, if-eket célszerű szétválasztani
- Output decode, ahány kimenet, annyi if (ez multiplexert hoz létre)
- Mindig legyen else ág!
- Ne csinálj latch-et állapotgép helyett, azaz a next state ne maradjon ugyanaz véletlenül, hanem legyen átadva ugyankként. (Latch problémája, hogy sokmindentől függ a működése, és bizonytalan a visszacsatolás miatt)
- D FF-eket szeretjük
- Érdemes az inputokat eltárolni registerbe, hogy minimális legyen a késleltetés


# GHDL commands
(use them in the src folder from cmd)


## Compiling from cmd:
 - Create `work` and `output` directory in the `src` folder
 - Run:\
 `cmd /V /C "set entity=game_of_life_tb&& ghdl -a --ieee=synopsys --std=08 --workdir=work *.vhd && ghdl -m --ieee=synopsys --std=08 --workdir=work !entity! && ghdl -r --ieee=synopsys --std=08 --workdir=work !entity! --vcd="output/!entity!".vcd"` 

## GTKWave from cmd:
- Open wave file:\
`$ gtkwave output/game_of_life_tb.vcd` 

_Note: GTKWave cannot output custom type signals (only bits) on the waveforms._

## Basic cmd commands:
- Change drive:\
`$ d:`
- Change drive, then cd to folder:\
`$ cd /d C:\Users`
- List content of folder:\
`$ dir`