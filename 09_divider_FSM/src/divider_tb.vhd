LIBRARY IEEE;
USE IEEE.std_logic_1164.ALL;
use IEEE.std_logic_unsigned.all;

ENTITY divider_tb IS
END divider_tb;

ARCHITECTURE arch OF divider_tb IS
    COMPONENT divider IS
        PORT (
            A : IN STD_LOGIC_VECTOR(2 DOWNTO 0);
            B : IN STD_LOGIC_VECTOR(2 DOWNTO 0);
            clk : IN STD_LOGIC;
            start : IN STD_LOGIC;
            Q_out : OUT STD_LOGIC_VECTOR(6 DOWNTO 0);
            R_out : OUT STD_LOGIC_VECTOR(2 DOWNTO 0)
        );
    END COMPONENT;

    SIGNAL A : STD_LOGIC_VECTOR(2 DOWNTO 0) := "111";
    SIGNAL B : STD_LOGIC_VECTOR(2 DOWNTO 0) := "011";
    SIGNAL clk : STD_LOGIC := '0';
    SIGNAL start : STD_LOGIC := '0';
BEGIN
    uut : divider PORT MAP(
        A => A,
        B => B,
        clk => clk,
        start => start
    );

    clk_gen : PROCESS
    BEGIN     
        FOR i IN 0 TO 100 LOOP
            clk <= '0';
            WAIT FOR 1 ns;
            clk <= '1';
            WAIT FOR 1 ns;
        END LOOP;
        wait;
    END PROCESS;

    stim : PROCESS
    BEGIN 
        WAIT FOR 100 ns;
        start <= '1';
        WAIT FOR 5 ns;
        start <= '0';
        WAIT;
    END PROCESS;
END architecture; -- arch