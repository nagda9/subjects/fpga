library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.std_logic_unsigned.all;

entity divider is
  port (
    A : in std_logic_vector(2 downto 0);
    B : in std_logic_vector(2 downto 0);
    clk : in std_logic;
    start : in std_logic;
    Q_out : out std_logic_vector(6 downto 0);
    R_out : out std_logic_vector(2 downto 0)
  ) ;
end divider ;

architecture arch of divider is
    signal x : std_logic_vector(2 downto 0) := "000";
    signal y : std_logic_vector(2 downto 0) := "000";
    signal q : std_logic_vector(6 downto 0) := "0000000";

    constant init : std_logic_vector(2 downto 0) := "001";
    constant compute : std_logic_vector(2 downto 0) := "010";
    constant done : std_logic_vector(2 downto 0) := "100";
    signal state : std_logic_vector(2 downto 0) := "001";
begin
    process (clk, start)
    begin
        if rising_edge(clk) then
            case( state ) is
                when init =>
                    x <= A;
                    y <= B;
                    
                    if (start = '1') then
                        state <= compute;
                    else 
                        state <= init;
                    end if;
                when compute =>
                    if (x >= y) then 
                        x <= x-y;
                        q <= q + 1;
                    end if;
                    
                    if (x >= y) then
                        state <= compute;
                    else 
                        state <= done;
                    end if;
                when done =>
                    R_out <= x;
                    Q_out <= q;
                    state <= init;
                when others => 
                    state <= init;
            end case ;
        end if;
    end process;
end architecture ; -- arch