# Configurations for windows

## GHDL install
- get the latest binary version from the git repo (ghdl-0.37-mingw32-mcode.zip):\
 https://github.com/ghdl/ghdl/releases
- unzip it, than add GHDL folder to path variables
   - Search 'Edit the system environment variables' 
   - Go to: Advanced | Environment variables | User variables | Path | Edit | New
   - Add the bin folder in the GHDL folder to the variables


## GTKWave install 
- get the latest binary version from the source forge project\
  https://sourceforge.net/projects/gtkwave/files/
- do the same steps with the GHDL folder


## GHDL commands
(use them in the src folder from cmd:\
 `$ cd /d D:\Studies\FPGA\` )
 - Create `work` and `output` directory in the `src` folder
 - Replace `<entity_name>` and run:\
 `$ cmd /V /C "set entity=<entity_name>&& ghdl -a --ieee=synopsys --std=08 --workdir=work *.vhd && ghdl -m --ieee=synopsys --std=08 --workdir=work !entity! && ghdl -r --ieee=synopsys --std=08 --workdir=work !entity! --vcd="output/!entity!".vcd"` 

## GTKWave from cmd:
- Open wave file:\
`$ gtkwave output/<entity_name>.vcd` 

_Note: GTKWave cannot output custom type signals (only bits) on the waveforms._

## Basic cmd commands:
- Change drive:\
`$ d:`
- Change drive, then cd to folder:\
`$ cd /d C:\Users`
- List content of folder:\
`$ dir`