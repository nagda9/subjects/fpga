LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;
USE IEEE.NUMERIC_STD.ALL;

ENTITY vga_term IS
	PORT (
		v_sync : OUT STD_LOGIC;
		h_sync : OUT STD_LOGIC;
		vga_data : OUT STD_LOGIC_VECTOR(11 DOWNTO 0);
		data_sel : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
		clk : IN STD_LOGIC);
END ENTITY; -- vga_term

ARCHITECTURE Behavioral OF vga_term IS
	--640x480@60Hz clk 25.175MHz (~800*525*60=25,200,000Hz)
	CONSTANT CHARACTER_WIDTH : INTEGER := 8;

	CONSTANT HOR_TOTAL_TIME : INTEGER := 100 * CHARACTER_WIDTH;
	CONSTANT HOR_ADDR_TIME : INTEGER := 80 * CHARACTER_WIDTH;
	CONSTANT HOR_BLANK_TIME : INTEGER := 20 * CHARACTER_WIDTH;

	CONSTANT H_FRONT_PORCH : INTEGER := 2 * CHARACTER_WIDTH;
	CONSTANT HOR_SYNC_TIME : INTEGER := 12 * CHARACTER_WIDTH;
	CONSTANT H_BACK_PORCH : INTEGER := 6 * CHARACTER_WIDTH;

	CONSTANT VER_TOTAL_TIME : INTEGER := 525;
	CONSTANT VER_ADDR_TIME : INTEGER := 480;
	CONSTANT VER_BLANK_TIME : INTEGER := 45;

	CONSTANT V_FRONT_PORCH : INTEGER := 10;
	CONSTANT VER_SYNC_TIME : INTEGER := 2;
	CONSTANT V_BACK_PORCH : INTEGER := 33;

	SIGNAL HCount : unsigned(10 DOWNTO 0);
	SIGNAL VCount : unsigned(10 DOWNTO 0);
	SIGNAL HSync : STD_LOGIC;
	SIGNAL VSync : STD_LOGIC;
	SIGNAL Comp : STD_LOGIC_VECTOR(5 DOWNTO 0);
	SIGNAL Colour : STD_LOGIC_VECTOR(11 DOWNTO 0);

	TYPE font_rom_hex_type IS ARRAY (0 TO 16 * 8 - 1) OF STD_LOGIC_VECTOR(7 DOWNTO 0);
	CONSTANT font_rom_hex : font_rom_hex_type := (
		X"3e", X"63", X"73", X"7b", X"6f", X"67", X"3e", X"00",
		X"0c", X"0e", X"0c", X"0c", X"0c", X"0c", X"3f", X"00",
		X"1e", X"33", X"30", X"1c", X"06", X"33", X"3f", X"00",
		X"1e", X"33", X"30", X"1c", X"30", X"33", X"1e", X"00",
		X"38", X"3c", X"36", X"33", X"7f", X"30", X"78", X"00",
		X"3f", X"03", X"1f", X"30", X"30", X"33", X"1e", X"00",
		X"1c", X"06", X"03", X"1f", X"33", X"33", X"1e", X"00",
		X"3f", X"33", X"30", X"18", X"0c", X"0c", X"0c", X"00",
		X"1e", X"33", X"33", X"1e", X"33", X"33", X"1e", X"00",
		X"1e", X"33", X"33", X"3e", X"30", X"18", X"0e", X"00",
		X"0c", X"1e", X"33", X"33", X"3f", X"33", X"33", X"00",
		X"3f", X"66", X"66", X"3e", X"66", X"66", X"3f", X"00",
		X"3c", X"66", X"03", X"03", X"03", X"66", X"3c", X"00",
		X"1f", X"36", X"66", X"66", X"66", X"36", X"1f", X"00",
		X"7f", X"46", X"16", X"1e", X"16", X"46", X"7f", X"00",
		X"7f", X"46", X"16", X"1e", X"16", X"06", X"0f", X"00"
	);

	TYPE font_rom_type IS ARRAY (0 TO 128 * 8 - 1) OF STD_LOGIC_VECTOR(7 DOWNTO 0);
	CONSTANT font_rom : font_rom_type := (
		X"00", X"00", X"00", X"00", X"00", X"00", X"00", X"00",
		X"00", X"00", X"00", X"00", X"00", X"00", X"00", X"00",
		X"00", X"00", X"00", X"00", X"00", X"00", X"00", X"00",
		X"00", X"00", X"00", X"00", X"00", X"00", X"00", X"00",
		X"00", X"00", X"00", X"00", X"00", X"00", X"00", X"00",
		X"00", X"00", X"00", X"00", X"00", X"00", X"00", X"00",
		X"00", X"00", X"00", X"00", X"00", X"00", X"00", X"00",
		X"00", X"00", X"00", X"00", X"00", X"00", X"00", X"00",
		X"00", X"00", X"00", X"00", X"00", X"00", X"00", X"00",
		X"00", X"00", X"00", X"00", X"00", X"00", X"00", X"00",
		X"00", X"00", X"00", X"00", X"00", X"00", X"00", X"00",
		X"00", X"00", X"00", X"00", X"00", X"00", X"00", X"00",
		X"00", X"00", X"00", X"00", X"00", X"00", X"00", X"00",
		X"00", X"00", X"00", X"00", X"00", X"00", X"00", X"00",
		X"00", X"00", X"00", X"00", X"00", X"00", X"00", X"00",
		X"00", X"00", X"00", X"00", X"00", X"00", X"00", X"00",
		X"00", X"00", X"00", X"00", X"00", X"00", X"00", X"00",
		X"00", X"00", X"00", X"00", X"00", X"00", X"00", X"00",
		X"00", X"00", X"00", X"00", X"00", X"00", X"00", X"00",
		X"00", X"00", X"00", X"00", X"00", X"00", X"00", X"00",
		X"00", X"00", X"00", X"00", X"00", X"00", X"00", X"00",
		X"00", X"00", X"00", X"00", X"00", X"00", X"00", X"00",
		X"00", X"00", X"00", X"00", X"00", X"00", X"00", X"00",
		X"00", X"00", X"00", X"00", X"00", X"00", X"00", X"00",
		X"00", X"00", X"00", X"00", X"00", X"00", X"00", X"00",
		X"00", X"00", X"00", X"00", X"00", X"00", X"00", X"00",
		X"00", X"00", X"00", X"00", X"00", X"00", X"00", X"00",
		X"00", X"00", X"00", X"00", X"00", X"00", X"00", X"00",
		X"00", X"00", X"00", X"00", X"00", X"00", X"00", X"00",
		X"00", X"00", X"00", X"00", X"00", X"00", X"00", X"00",
		X"00", X"00", X"00", X"00", X"00", X"00", X"00", X"00",
		X"00", X"00", X"00", X"00", X"00", X"00", X"00", X"00",
		X"00", X"00", X"00", X"00", X"00", X"00", X"00", X"00",
		X"18", X"3c", X"3c", X"18", X"18", X"00", X"18", X"00",
		X"36", X"36", X"00", X"00", X"00", X"00", X"00", X"00",
		X"36", X"36", X"7f", X"36", X"7f", X"36", X"36", X"00",
		X"0c", X"3e", X"03", X"1e", X"30", X"1f", X"0c", X"00",
		X"00", X"63", X"33", X"18", X"0c", X"66", X"63", X"00",
		X"1c", X"36", X"1c", X"6e", X"3b", X"33", X"6e", X"00",
		X"06", X"06", X"03", X"00", X"00", X"00", X"00", X"00",
		X"18", X"0c", X"06", X"06", X"06", X"0c", X"18", X"00",
		X"06", X"0c", X"18", X"18", X"18", X"0c", X"06", X"00",
		X"00", X"66", X"3c", X"ff", X"3c", X"66", X"00", X"00",
		X"00", X"0c", X"0c", X"3f", X"0c", X"0c", X"00", X"00",
		X"00", X"00", X"00", X"00", X"00", X"0c", X"0c", X"06",
		X"00", X"00", X"00", X"3f", X"00", X"00", X"00", X"00",
		X"00", X"00", X"00", X"00", X"00", X"0c", X"0c", X"00",
		X"60", X"30", X"18", X"0c", X"06", X"03", X"01", X"00",
		X"3e", X"63", X"73", X"7b", X"6f", X"67", X"3e", X"00",
		X"0c", X"0e", X"0c", X"0c", X"0c", X"0c", X"3f", X"00",
		X"1e", X"33", X"30", X"1c", X"06", X"33", X"3f", X"00",
		X"1e", X"33", X"30", X"1c", X"30", X"33", X"1e", X"00",
		X"38", X"3c", X"36", X"33", X"7f", X"30", X"78", X"00",
		X"3f", X"03", X"1f", X"30", X"30", X"33", X"1e", X"00",
		X"1c", X"06", X"03", X"1f", X"33", X"33", X"1e", X"00",
		X"3f", X"33", X"30", X"18", X"0c", X"0c", X"0c", X"00",
		X"1e", X"33", X"33", X"1e", X"33", X"33", X"1e", X"00",
		X"1e", X"33", X"33", X"3e", X"30", X"18", X"0e", X"00",
		X"00", X"0c", X"0c", X"00", X"00", X"0c", X"0c", X"00",
		X"00", X"0c", X"0c", X"00", X"00", X"0c", X"0c", X"06",
		X"18", X"0c", X"06", X"03", X"06", X"0c", X"18", X"00",
		X"00", X"00", X"3f", X"00", X"00", X"3f", X"00", X"00",
		X"06", X"0c", X"18", X"30", X"18", X"0c", X"06", X"00",
		X"1e", X"33", X"30", X"18", X"0c", X"00", X"0c", X"00",
		X"3e", X"63", X"7b", X"7b", X"7b", X"03", X"1e", X"00",
		X"0c", X"1e", X"33", X"33", X"3f", X"33", X"33", X"00",
		X"3f", X"66", X"66", X"3e", X"66", X"66", X"3f", X"00",
		X"3c", X"66", X"03", X"03", X"03", X"66", X"3c", X"00",
		X"1f", X"36", X"66", X"66", X"66", X"36", X"1f", X"00",
		X"7f", X"46", X"16", X"1e", X"16", X"46", X"7f", X"00",
		X"7f", X"46", X"16", X"1e", X"16", X"06", X"0f", X"00",
		X"3c", X"66", X"03", X"03", X"73", X"66", X"7c", X"00",
		X"33", X"33", X"33", X"3f", X"33", X"33", X"33", X"00",
		X"1e", X"0c", X"0c", X"0c", X"0c", X"0c", X"1e", X"00",
		X"78", X"30", X"30", X"30", X"33", X"33", X"1e", X"00",
		X"67", X"66", X"36", X"1e", X"36", X"66", X"67", X"00",
		X"0f", X"06", X"06", X"06", X"46", X"66", X"7f", X"00",
		X"63", X"77", X"7f", X"7f", X"6b", X"63", X"63", X"00",
		X"63", X"67", X"6f", X"7b", X"73", X"63", X"63", X"00",
		X"1c", X"36", X"63", X"63", X"63", X"36", X"1c", X"00",
		X"3f", X"66", X"66", X"3e", X"06", X"06", X"0f", X"00",
		X"1e", X"33", X"33", X"33", X"3b", X"1e", X"38", X"00",
		X"3f", X"66", X"66", X"3e", X"36", X"66", X"67", X"00",
		X"1e", X"33", X"07", X"0e", X"38", X"33", X"1e", X"00",
		X"3f", X"2d", X"0c", X"0c", X"0c", X"0c", X"1e", X"00",
		X"33", X"33", X"33", X"33", X"33", X"33", X"3f", X"00",
		X"33", X"33", X"33", X"33", X"33", X"1e", X"0c", X"00",
		X"63", X"63", X"63", X"6b", X"7f", X"77", X"63", X"00",
		X"63", X"63", X"36", X"1c", X"1c", X"36", X"63", X"00",
		X"33", X"33", X"33", X"1e", X"0c", X"0c", X"1e", X"00",
		X"7f", X"63", X"31", X"18", X"4c", X"66", X"7f", X"00",
		X"1e", X"06", X"06", X"06", X"06", X"06", X"1e", X"00",
		X"03", X"06", X"0c", X"18", X"30", X"60", X"40", X"00",
		X"1e", X"18", X"18", X"18", X"18", X"18", X"1e", X"00",
		X"08", X"1c", X"36", X"63", X"00", X"00", X"00", X"00",
		X"00", X"00", X"00", X"00", X"00", X"00", X"00", X"ff",
		X"0c", X"0c", X"18", X"00", X"00", X"00", X"00", X"00",
		X"00", X"00", X"1e", X"30", X"3e", X"33", X"6e", X"00",
		X"07", X"06", X"06", X"3e", X"66", X"66", X"3b", X"00",
		X"00", X"00", X"1e", X"33", X"03", X"33", X"1e", X"00",
		X"38", X"30", X"30", X"3e", X"33", X"33", X"6e", X"00",
		X"00", X"00", X"1e", X"33", X"3f", X"03", X"1e", X"00",
		X"1c", X"36", X"06", X"0f", X"06", X"06", X"0f", X"00",
		X"00", X"00", X"6e", X"33", X"33", X"3e", X"30", X"1f",
		X"07", X"06", X"36", X"6e", X"66", X"66", X"67", X"00",
		X"0c", X"00", X"0e", X"0c", X"0c", X"0c", X"1e", X"00",
		X"30", X"00", X"30", X"30", X"30", X"33", X"33", X"1e",
		X"07", X"06", X"66", X"36", X"1e", X"36", X"67", X"00",
		X"0e", X"0c", X"0c", X"0c", X"0c", X"0c", X"1e", X"00",
		X"00", X"00", X"33", X"7f", X"7f", X"6b", X"63", X"00",
		X"00", X"00", X"1f", X"33", X"33", X"33", X"33", X"00",
		X"00", X"00", X"1e", X"33", X"33", X"33", X"1e", X"00",
		X"00", X"00", X"3b", X"66", X"66", X"3e", X"06", X"0f",
		X"00", X"00", X"6e", X"33", X"33", X"3e", X"30", X"78",
		X"00", X"00", X"3b", X"6e", X"66", X"06", X"0f", X"00",
		X"00", X"00", X"3e", X"03", X"1e", X"30", X"1f", X"00",
		X"08", X"0c", X"3e", X"0c", X"0c", X"2c", X"18", X"00",
		X"00", X"00", X"33", X"33", X"33", X"33", X"6e", X"00",
		X"00", X"00", X"33", X"33", X"33", X"1e", X"0c", X"00",
		X"00", X"00", X"63", X"6b", X"7f", X"7f", X"36", X"00",
		X"00", X"00", X"63", X"36", X"1c", X"36", X"63", X"00",
		X"00", X"00", X"33", X"33", X"33", X"3e", X"30", X"1f",
		X"00", X"00", X"3f", X"19", X"0c", X"26", X"3f", X"00",
		X"38", X"0c", X"0c", X"07", X"0c", X"0c", X"38", X"00",
		X"18", X"18", X"18", X"00", X"18", X"18", X"18", X"00",
		X"07", X"0c", X"0c", X"38", X"0c", X"0c", X"07", X"00",
		X"6e", X"3b", X"00", X"00", X"00", X"00", X"00", X"00",
		X"00", X"00", X"00", X"00", X"00", X"00", X"00", X"00"
	);

	SIGNAL font_rom_reg : STD_LOGIC_VECTOR(7 DOWNTO 0);
BEGIN
	PROCESS (clk)
	BEGIN
		IF clk'event AND clk = '1' THEN
			IF HCount = HOR_TOTAL_TIME - 1 THEN
				HCount <= (OTHERS => '0');
				IF VCount = VER_TOTAL_TIME - 1 THEN
					VCount <= (OTHERS => '0');
				ELSE
					VCount <= VCount + 1;
				END IF;
			ELSE
				HCount <= HCount + 1;
			END IF;
			IF HCount = HOR_ADDR_TIME + H_FRONT_PORCH + HOR_SYNC_TIME - 1 THEN
				HSync <= '1';
			ELSIF HCount = HOR_ADDR_TIME + H_FRONT_PORCH - 1 THEN
				HSync <= '0';
			END IF;
			IF HCount = HOR_TOTAL_TIME - 1 THEN
				IF VCount = VER_ADDR_TIME + V_FRONT_PORCH + VER_SYNC_TIME - 1 THEN
					VSync <= '1';
				ELSIF VCount = VER_ADDR_TIME + V_FRONT_PORCH - 1 THEN
					VSync <= '0';
				END IF;
			END IF;
			IF unsigned(HCount(2 DOWNTO 0)) = 0 THEN
				--font_rom_reg <= font_rom_hex(to_integer(unsigned(data_sel(3 downto 0)) & VCount(2 downto 0)));	
				--font_rom_reg <= font_rom(to_integer(unsigned(data_sel(6 downto 0)) & VCount(2 downto 0)));	
				font_rom_reg <= font_rom(to_integer(VCount(3) & HCount(8 DOWNTO 3) & VCount(2 DOWNTO 0)));
			ELSE
				font_rom_reg <= '0' & font_rom_reg(7 DOWNTO 1);
			END IF;
			IF HCount < HOR_ADDR_TIME AND VCount < VER_ADDR_TIME THEN
				--vga_data <= Colour(11 downto 0);--test pattern
				vga_data <= (OTHERS => font_rom_reg(0));
			ELSE
				vga_data <= (OTHERS => '0');
			END IF;
		END IF;
	END PROCESS;
	
	h_sync <= HSync;
	v_sync <= VSync;
	Comp <= STD_LOGIC_VECTOR(HCount(5 DOWNTO 0) XOR VCount(5 DOWNTO 0));
	Colour <= Comp & Comp;

END Behavioral;