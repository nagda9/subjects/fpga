library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity vga_term_tb is
end vga_term_tb ;

architecture arch of vga_term_tb is
    component vga_term is
        Port ( 
            v_sync : out STD_LOGIC;
            h_sync : out STD_LOGIC;
            vga_data : out STD_LOGIC_VECTOR(11 downto 0);
            data_sel : in STD_LOGIC_VECTOR(7 downto 0);
            clk : in STD_LOGIC);
    end component;

    signal v_sync_out : std_logic;
    signal h_sync_out : std_logic;
    signal vga_data_out : std_logic_vector(11 downto 0);
    signal data_sel_in : std_logic_vector(7 downto 0) := "01001101";
    signal clk_in : std_logic := '0';
begin
    uut : vga_term port map (
        v_sync => v_sync_out,
        h_sync => h_sync_out,
        vga_data => vga_data_out,
        data_sel => data_sel_in,
        clk => clk_in 
    );
    
	stim : PROCESS
	BEGIN
		WAIT;
    END PROCESS;
    
    clk_gen : process
    begin
        for i in 1 to 10 loop
            clk_in <= '0';
            WAIT FOR 5 ns;
            clk_in <= '1';
            WAIT FOR 5 ns;
        end loop;
        wait;
    end process; -- stim

end architecture ; -- arch